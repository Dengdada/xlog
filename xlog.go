package log

// Default represents the default logger
var Default *logService

func init() {
	Default = &logService{
		loggers: make(map[string]*Logger),
	}

	err := NewLogger("Default", "console", "")
	if err != nil {
		panic("create default logger failed")
	}
}

// Debug log the debug log
func Debug(format string, args ...interface{}) {
	Default.Debug(format, args...)
}

// Trace log the trace log
func Trace(format string, args ...interface{}) {
	Default.Trace(format, args...)
}

// Info log the info log
func Info(format string, args ...interface{}) {
	Default.Info(format, args...)
}

// Warn log the warn log
func Warn(format string, args ...interface{}) {
	Default.Warn(format, args...)
}

// Error log the error log
func Error(format string, args ...interface{}) {
	Default.Error(format, args...)
}

// Critical log the error log
func Critical(format string, args ...interface{}) {
	Default.Critical(format, args...)
}

// Fatal log the error log
func Fatal(format string, args ...interface{}) {
	Default.Fatal(format, args...)
}

// Close close the log
func Close() {
	Default.Close()
}
