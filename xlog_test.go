package log

import (
	"fmt"
	"testing"
)

func TestXLOG(t *testing.T) {
	defer Close()

	NewLogger("Default", "console", `{"level":"debug"}`)
	NewLogger("file", "file", `{"filename":"xlog.log", "level":"info"}`)
	Debug("debug log: %d", 1)
	Info("info log: %v", "a")
	Warn("test")
	Error("this is an error")
	Critical("this is a critial")
	Fatal("this is a fatal")
	fmt.Println("this will not be displayed")
}
