module gitea.com/lunny/xlog

go 1.16

require (
	github.com/mattn/go-colorable v0.1.8
	github.com/natefinch/lumberjack v2.0.0+incompatible
	go.uber.org/zap v1.16.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
