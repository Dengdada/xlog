package log

type logService struct {
	loggers map[string]*Logger
}

// Debug log the debug log
func (l *logService) Debug(format string, args ...interface{}) {
	for _, logger := range l.loggers {
		logger.Debug(format, args...)
	}
}

// Trace log the trace log
func (l *logService) Trace(format string, args ...interface{}) {
	for _, logger := range l.loggers {
		logger.Trace(format, args...)
	}
}

// Info log the info log
func (l *logService) Info(format string, args ...interface{}) {
	for _, logger := range l.loggers {
		logger.Info(format, args...)
	}
}

// Error log the error log
func (l *logService) Error(format string, args ...interface{}) {
	for _, logger := range l.loggers {
		logger.Error(format, args...)
	}
}

// Warn log the warn log
func (l *logService) Warn(format string, args ...interface{}) {
	for _, logger := range l.loggers {
		logger.Warn(format, args...)
	}
}

// Critical log the critical error log
func (l *logService) Critical(format string, args ...interface{}) {
	for _, logger := range l.loggers {
		logger.Critical(format, args...)
	}
}

// Fatal log the fatal log
func (l *logService) Fatal(format string, args ...interface{}) {
	for _, logger := range l.loggers {
		logger.Fatal(format, args...)
	}
}

func (l *logService) Close() {
	for _, logger := range l.loggers {
		logger.Close()
	}
}
